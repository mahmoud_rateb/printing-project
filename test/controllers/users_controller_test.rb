require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @other_user = users(:archer)
  end

  test "should get new" do
    get users_new_url
    assert_response :success
  end

  test "Should not allow admin to be set to true for @other_user" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user),params: { user: {admin: true}}
    assert_not @other_user.admin?
  end

end

require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "Invalid signup info" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: {
        user: {
          name: "",
          email: "rateb@hotmail.com",
          mobile_no: "aadd",
          password: "123dfab",
          password_confirmation: "bar"
        }
      }
      assert_template 'users/new'
      assert_select 'div#error_explanation'
      assert_select 'div.field_with_errors'
    end
  end

  test "valid signup and make sure that user gets logged in" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: {
        user: {
          name: "Mohannad",
          email: "rateb+331@aucegypt.edu",
          password: "123456",
          password_confirmation: "123456",
        }
      }
      assert_equal 1, ActionMailer::Base.deliveries.size
      user = assigns(:user)
      assert_not user.activated?

      # Invalid activation token
      get edit_account_activation_path("invalid token", email: user.email)
      assert_not is_logged_in?

      # Valid token, wrong email
      get edit_account_activation_path(user.activation_token, email: 'wrong')
      assert_not is_logged_in?

      # Valid activation token
      get edit_account_activation_path(user.activation_token, email: user.email)
      assert user.reload.activated?
      follow_redirect!
      assert_select "title", "Upload file | The Easiest way to print and frame photos in Egypt"
      assert is_logged_in?
    end
  end

end

# frozen_string_literal: true

require 'test_helper'

class UploadPhotosFlowTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test 'Can reach upload page if not logged in as a guest user' do
    delete logout_path
    get upload_path
    assert_equal '/en/upload', path
  end

  test 'Can see the upload page when logged in' do
    log_in_as(@user)
    get upload_path
    assert_select 'title', 'Upload file | The Easiest way to print and frame photos in Egypt'
  end
end

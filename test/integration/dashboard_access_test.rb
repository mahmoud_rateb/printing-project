# frozen_string_literal: true

require 'test_helper'

class DashboardAccessTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @admin_user_michael = users(:michael)
    @other_user = users(:archer)
  end

  test "Can't access dashboard page if not admin user" do
    log_in_as(@other_user)
    get backend_dashboard_path
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "Can't access backend order if not admin user" do
    log_in_as(@other_user)
    get backend_dashboard_path
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "Can access dashboard if admin user without flash message" do
    log_in_as(@admin_user_michael)
    get backend_dashboard_path
    assert flash.empty?
    assert_template 'orders/backend_dashboard'
  end
end

require 'test_helper'

class ChoosePhotoSizesTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "Navigating to this page redirects if no photos were uploaded" do
    log_in_as(@user)
    get orders_choose_sizes_path
    assert_redirected_to upload_path
  end
end

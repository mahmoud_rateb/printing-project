# frozen_string_literal: true

require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test 'unsuccessful edit' do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: {
      user: {
        name: '',
        email: 'foo@invalid',
        password: 'foo',
        password_confirmation: 'bar'
      }
    }
    assert_template 'users/edit'
  end

  test 'successful edit' do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = 'Aly'
    email = 'mohannad+10@student.guc.edu.eg'
    patch user_path(@user), params: {
      user: {
        name: name,
        email: email,
        mobile_no: @user[:mobile_no],
        password: '',
        password_confirmation: ''
      }
    }
    @user.reload
    assert_not flash.empty?
    assert_equal name, @user.name
    assert_equal email, @user.email
  end

  test 'should redirect edit when not logged in' do
    get edit_user_path(id: @user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test 'should redirect update when not logged in' do
    name = 'Aly'
    email = 'mohannad+10@student.guc.edu.eg'
    patch user_path(id: @user), params: {
      user: {
        name: name,
        email: email,
        mobile_no: @user[:mobile_no],
        study_year: @user[:study_year],
        password: '',
        password_confirmation: ''
      }
    }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test 'should redirect edit when logged in with wrong user' do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test 'should redirect update when logged in with the wrong user' do
    name = 'Aly'
    email = 'mohannad+10@student.guc.edu.eg'
    log_in_as(@other_user)
    patch user_path(@user), params: {
      user: {
        name: name,
        email: email,
        mobile_no: @user[:mobile_no],
        study_year: @user[:study_year],
        password: '',
        password_confirmation: ''
      }
    }
    assert_not flash.empty?
    assert_redirected_to root_url
  end
end

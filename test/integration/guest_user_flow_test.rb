# frozen_string_literal: true

require "test_helper"

class GuestUserFlowTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test 'First make sure user isnt logged in' do
    get root_path
    assert_select 'a[href=?]', login_path
    assert_select 'a[href=?]', signup_path
  end

  test 'Then test pressing getting started takes you to upload photos page' do
    get upload_path
    assert_equal '/upload', path
  end
end

require 'test_helper'

class PhotoTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @myfile = @user.photos.build(user_id: @user.id)
  end

  test "should not be valid" do
  	assert_not @myfile.valid?
  end

  test "user id should be present" do
    @myfile.user_id = nil
    assert_not @myfile.valid?
  end

  test "File can not be empty for uploaded file" do
    @myfile.path = nil
    assert_not @myfile.valid?
  end

end

# frozen_string_literal: true

require 'test_helper'

class PhotoSizeTest < ActiveSupport::TestCase
  def setup
    @photo_size = PhotoSize.new(size: '5*10', selling_price: 10)
  end

  test 'should be valid' do
    assert @photo_size.valid?
  end

  test 'size should be present' do
    @photo_size.size = ''
    assert_not @photo_size.valid?
  end

  test 'selling price should be present' do
    @photo_size.selling_price = nil
    assert_not @photo_size.valid?
  end
end

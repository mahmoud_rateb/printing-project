require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @area_1 = areas(:Maadi)
    @address_1 = @user.addresses.build
    @address_1.area = @area_1
    @address_1.street_name = "Street 10"
    @address_1.building_no = "10"
    @address_1.mobile_no = "01004100209"
    @address_1.save
  end

  test "phone does not have to be unique" do
    @address_2 = @user.addresses.build
    @address_2.name = "Aly"
    @address_2.area = @area_1
    @address_2.street_name = "Street 10"
    @address_2.building_no = "10"
    @address_2.mobile_no = "01004100209"

    assert @address_2.valid?
  end

end

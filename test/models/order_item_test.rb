require 'test_helper'

class OrderItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:michael)
    photo = photos(:one)
    photo_size = photo_sizes(:one)
    @order = @user.orders.build
    @order_item = @order.order_items.new(quantity: 1, selling_price: 30)
    @order_item.photo = photo
    @order_item.photo_size = photo_size
  end

  test "Should be valid" do
    assert @order_item.valid?
  end

  test "Photo reference cant be null" do
    @order_item.photo = nil
    assert_not @order_item.valid?
  end

  test "Selling price can't be empty." do
  	@order_item.selling_price = nil
  	assert_not @order_item.valid?
  end

  test "Quantity can't be empty" do
  	@order_item.quantity = nil
  	assert_not @order_item.valid?
  end
end

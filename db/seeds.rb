# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user_1 = User.find_by(email: "rateb@aucegypt.edu")
if(!user_1)
  User.create!(name:  "Example User",
               email: "rateb@aucegypt.edu",
               mobile_no: "01004100209",
               password:              "123456",
               password_confirmation: "123456",
               activated: true,
               activated_at: Time.zone.now,
               admin: true
              )
end

# if (User.all.count < 100)
#   99.times do |n|
#     name  = Faker::Name.name
#     email = Faker::Internet.unique.email
#     mobile = Faker::Base.unique.regexify(/01[0-2]{1}[0-9]{8}/)
#     password = "password"
#     User.create!(name:  name,
#                  email: email,
#                  mobile_no: mobile,
#                  password: password,
#                  password_confirmation: password,
#                  activated: true,
#                  activated_at: Time.zone.now)
#   end
# end

def update_price_for_size(size, price)
  photo_size = PhotoSize.find_by(size: size, is_frame: false)
  if !photo_size
    PhotoSize.create!(size: size,
      selling_price: price, is_frame: false)
    else photo_size.selling_price = price
      photo_size.save
    end
end

def update_price_for_frame(size, price, cost_price, is_matted)
  frame_size = FrameSize.where(size: size, is_matted: is_matted).first
  if !frame_size
    FrameSize.create!(size: size, selling_price: price, cost_price: cost_price, is_matted: is_matted)
  else frame_size.selling_price = price
    frame_size.cost_price = cost_price
    frame_size.save
  end
end

#Seeding the photo_sizes table
photo_sizes_prices = { "10x15" => 4, "13x18" => 6, "15x21" => 9, "20x25" => 30, " 20x30" => 40, "30x40" => 65}
photo_sizes_prices.each {
  |size,price| update_price_for_size(size, price)
}

frames_prices = [
  {:size => "15x21", :selling_price => 95, :cost_price => 44.5, :is_matted => false },
  {:size => "25x31", :selling_price => 139, :cost_price => 64.5, :is_matted => true },
  {:size => "20x25", :selling_price => 139, :cost_price => nil, :is_matted => false },
  {:size => "30x40", :selling_price => 219, :cost_price => 125, :is_matted => false},
  {:size => "30x40",:selling_price => 219, :cost_price => 120, :is_matted => true},
  {:size => "40x50",:selling_price => 279, :cost_price => 185, :is_matted => false},
  {:size => "40x50",:selling_price => 289, :cost_price => 180, :is_matted => true}
]

frames_prices.each {
  |details| update_price_for_frame(details[:size], details[:selling_price], details[:cost_price], details[:is_matted])
}

#Seeding the Frames table
file_paths = Dir.glob("app/assets/images/Frames/**/*")
file_paths.each do |path|
  name = File.basename(path,'.png')
  if(name == "Classic" || name == "Ever")
    is_matted = true
  else is_matted = false
  end


  existing_frame = Frame.find_by(name: name)
  if !existing_frame
    Frame.create!(
      name: name,
      photo_path: path,
      is_matted: is_matted
    )
  else existing_frame.is_matted = is_matted
    existing_frame.save
  end
end

#Seeding Photos table with 2 photos
my_user = User.find(1)
if(my_user.photos.count < 2)
  my_user.photos.create!(
    name: "first.jpg",
  path: "https://printpicsnowphotos.s3-eu-west-1.amazonaws.com/uploads3/0802ec8c71eb508b8402648d2f76e745.jpg")
  my_user.photos.create!(
    name: Faker::Name.name,
    path: "https://printpicsnowphotos.s3-eu-west-1.amazonaws.com/uploads3/0d6c6990b4bf72aeec903ea37010bfc1.jpg"
  )
end

#Seeding the cities table
open("db/egypt_cities.txt") do |cities|
  cities.read.each_line do |line|
    city_name = line.squish
    if !City.find_by(name: city_name)
      City.create!(:name => city_name)
    end
  end
end

#Seeding the areas table
cairo = City.find_by(name: "Cairo")
open("db/cairo_areas.txt") do |areas|
  areas.read.each_line do |line|
    area_name = line.squish
    if !Area.find_by(name: area_name)
      cairo.areas.create(:name => area_name)
    end
  end
end

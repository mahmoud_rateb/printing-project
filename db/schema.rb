# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210808162238) do

  create_table "addresses", force: :cascade do |t|
    t.string "street_name"
    t.string "building_no"
    t.string "floor"
    t.string "flat"
    t.string "landmark"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "mobile_no"
    t.integer "area_id"
    t.string "name"
    t.index ["area_id"], name: "index_addresses_on_area_id"
    t.index ["user_id"], name: "index_addresses_on_user_id"
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_areas_on_city_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "frame_sizes", force: :cascade do |t|
    t.string "size"
    t.decimal "selling_price", precision: 10, scale: 2
    t.decimal "cost_price", precision: 10, scale: 2
    t.boolean "is_matted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "frames", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo_path"
    t.boolean "is_matted"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "order_id"
    t.integer "photo_size_id"
    t.integer "photo_id"
    t.integer "quantity"
    t.decimal "selling_price", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "frame_id"
    t.integer "frame_size_id"
    t.index ["frame_id"], name: "index_order_items_on_frame_id"
    t.index ["frame_size_id"], name: "index_order_items_on_frame_size_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["photo_id"], name: "index_order_items_on_photo_id"
    t.index ["photo_size_id"], name: "index_order_items_on_photo_size_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "status", limit: 2, default: 0
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "address_id"
    t.string "order_number"
    t.datetime "ordered_at"
    t.index ["address_id"], name: "index_orders_on_address_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "photo_sizes", force: :cascade do |t|
    t.string "size"
    t.decimal "selling_price", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_frame", default: false
  end

  create_table "photos", force: :cascade do |t|
    t.string "path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "name"
    t.string "thumbnail_path"
    t.index ["user_id"], name: "index_photos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "mobile_no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.boolean "admin", default: false
    t.boolean "guest"
    t.index ["email"], name: "index_users_on_email", unique: true, where: "guest = false"
    t.index ["mobile_no"], name: "index_users_on_mobile_no", unique: true
  end

end

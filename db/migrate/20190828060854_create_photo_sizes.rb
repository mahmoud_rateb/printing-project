class CreatePhotoSizes < ActiveRecord::Migration[5.1]
  def change
    create_table :photo_sizes do |t|
      t.string :size
      t.decimal :selling_price, precision: 10, scale: 2

      t.timestamps
    end
  end
end

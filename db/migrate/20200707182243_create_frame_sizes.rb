class CreateFrameSizes < ActiveRecord::Migration[5.1]
  def change
    create_table :frame_sizes do |t|
      t.string :size
      t.decimal :selling_price, precision: 10, scale: 2
      t.decimal :cost_price, precision: 10, scale: 2
      t.boolean :is_matted

      t.timestamps
    end
  end
end

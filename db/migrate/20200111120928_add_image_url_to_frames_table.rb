class AddImageUrlToFramesTable < ActiveRecord::Migration[5.1]
  def change
  	add_column :frames, :photo_path, :string
  end
end

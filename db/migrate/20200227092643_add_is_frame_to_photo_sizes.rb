class AddIsFrameToPhotoSizes < ActiveRecord::Migration[5.1]
  def change
    add_column :photo_sizes, :is_frame, :boolean
  end
end

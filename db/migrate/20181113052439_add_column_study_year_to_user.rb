class AddColumnStudyYearToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :study_year, :integer
  end
end

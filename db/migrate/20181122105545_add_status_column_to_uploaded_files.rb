class AddStatusColumnToUploadedFiles < ActiveRecord::Migration[5.1]
  def change
    add_column :uploaded_files, :status, :integer, :limit => 2, :default => 0
  end
end

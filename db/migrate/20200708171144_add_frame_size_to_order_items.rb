class AddFrameSizeToOrderItems < ActiveRecord::Migration[5.1]
  def change
    add_reference :order_items, :frame_size, foreign_key: true
  end
end

class AddUserToUploadedFiles < ActiveRecord::Migration[5.1]
  def change
    add_reference :uploaded_files, :user, foreign_key: true
  end
end

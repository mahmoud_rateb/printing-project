class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.references :order, foreign_key: true
      t.references :photo_size, foreign_key: true
      t.references :photo, foreign_key: true
      t.integer :quantity
      t.decimal :selling_price, precision: 10, scale: 2

      t.timestamps
    end
  end
end

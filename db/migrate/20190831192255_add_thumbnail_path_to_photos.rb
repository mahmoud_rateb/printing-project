class AddThumbnailPathToPhotos < ActiveRecord::Migration[5.1]
  def change
    add_column :photos, :thumbnail_path, :string
  end
end

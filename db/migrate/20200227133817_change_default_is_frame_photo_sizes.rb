class ChangeDefaultIsFramePhotoSizes < ActiveRecord::Migration[5.1]
  def change
    change_column_default :photo_sizes, :is_frame, false
  end
end

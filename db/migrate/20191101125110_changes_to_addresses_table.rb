class ChangesToAddressesTable < ActiveRecord::Migration[5.1]
  def change
  	remove_column :addresses, :city
  	remove_column :addresses, :area
  	add_column :addresses, :mobile_no, :string
  	add_reference :addresses, :area, foreign_key: true
  end
end

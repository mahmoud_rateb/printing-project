class AddIsMattedToFrames < ActiveRecord::Migration[5.1]
  def change
    add_column :frames, :is_matted, :boolean
  end
end

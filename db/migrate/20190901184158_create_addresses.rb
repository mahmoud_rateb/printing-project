class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :city
      t.string :area
      t.string :street_name
      t.string :building_no
      t.string :floor
      t.string :flat
      t.string :landmark
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

class RemoveStudyYearFromUser < ActiveRecord::Migration[5.1]
  def change
  	remove_column :users, :study_year
  end
end

class AlterTableToBecomeUploadedPhotos < ActiveRecord::Migration[5.1]
  def change
  	rename_column :uploaded_files, :uploaded_file , :path
  	remove_column :uploaded_files, :status
  	rename_table :uploaded_files, :photos
  end
end

class AddFrameToOrderItems < ActiveRecord::Migration[5.1]
  def change
  	add_reference :order_items, :frame, foreign_key: true
  end
end

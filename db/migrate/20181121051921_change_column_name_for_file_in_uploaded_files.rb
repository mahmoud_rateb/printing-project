class ChangeColumnNameForFileInUploadedFiles < ActiveRecord::Migration[5.1]
  def change
  	rename_column :uploaded_files, :location, :uploaded_file
  end
end

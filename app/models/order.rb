# frozen_string_literal: true

STATUS_VALUES = { 0 => 'CART', 1 => 'ORDERED', 2 => 'CONFIRMED', 3 => 'DELIVERED', 4 => 'CANCELED' }.freeze
class Order < ApplicationRecord
  belongs_to :user
  belongs_to :address, optional: true
  has_many :order_items, inverse_of: :order
  has_many :photos, through: :order_items
  has_many :photo_sizes, through: :order_items
  accepts_nested_attributes_for :order_items
  before_save :status_changed_to_ordered

  def total
    total = 0
    order_items.each do |item|
      total += item.quantity * item.selling_price
    end
    total
  end

  def status_word
    STATUS_VALUES[status]
  end

  def send_printing_email
    OrderMailer.printer_photos(self).deliver_now
  end

  # private

  def secure_order_token
    DateTime.now.strftime('%Y%m%d').to_s + '-' + rand(9999).to_s
  end

  def generate_order_number
    order_number = nil
    loop do
      order_number = secure_order_token
      break unless Order.exists?(order_number: order_number)
    end

    self.order_number = order_number
  end

  def status_changed_to_ordered
    return unless status_changed? && status == 1

    self.ordered_at = Time.now
    generate_order_number
  end
end

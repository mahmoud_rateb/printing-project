# frozen_string_literal: true

class FrameSize < ApplicationRecord
  has_many :order_items
  validates :size, presence: true
  validates :selling_price, presence: true

  def size_and_price
    I18n.t('currencies.size_and_price', width: size.split('x').first, height: size.split('x').second, selling_price: selling_price.to_s)
  end
end

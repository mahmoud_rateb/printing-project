class Cart < Order
  self.table_name = "orders"
  default_scope { where("orders.status = 0") }

  def place_order
    self.status = 1
    self.save
  end

  def move_items_to_other_user_cart(other_user)
    if order_items.any?
      order_items.update_all(order_id: other_user.cart.id)
    end
  end
end

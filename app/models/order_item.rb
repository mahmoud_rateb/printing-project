class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :photo_size, optional: true
  belongs_to :frame_size, optional: true
  belongs_to :photo
  belongs_to :frame, optional: true

  attr_accessor :item_size_id

  validates :photo, presence: true
  validates :order, presence: true
  validates :selling_price, presence: true
  validates :quantity, presence: true
end

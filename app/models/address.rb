class Address < ApplicationRecord
  belongs_to :user
  belongs_to :area
  has_many :orders

  validates_presence_of :name
  validates :area, presence: true
  validates :street_name, presence: true
  validates :building_no, presence: true
  VALID_PHONE_NUMBERS_REGEX = /\A01[0-2]{1}[0-9]{8}/
  validates :mobile_no, presence: true, format: { with: VALID_PHONE_NUMBERS_REGEX }

  def text_address
    street_name_handle = I18n.t('activerecord.attributes.address.street_name')
    building_no_handle = I18n.t('activerecord.attributes.address.building_no')
    flat_handle = I18n.t('activerecord.attributes.address.flat')
    landmark_handle = I18n.t('activerecord.attributes.address.landmark')
    address = "Name: #{name}"
  	address += "\n #{street_name_handle}: #{street_name}"
  	address += "\n #{building_no_handle}: #{building_no.to_s}" if building_no.present?
  	address += "\n #{flat_handle}: #{flat}" if flat.present?
  	address += "\n #{landmark_handle}: #{landmark}"  if landmark.present?
  	address += "\n" + self.area.name
  end

end

# frozen_string_literal: true

require 'mini_magick'

class Photo < ApplicationRecord
  belongs_to :user
  has_many :order_items

  validates :user_id, presence: true
  validates :path, presence: true

  def store_thumbnail_in_s3
    temp_img_path = store_image_temp_location
    resize_temp_file(temp_img_path)
    self.thumbnail_path = upload_thumbnail(temp_img_path)
    save
  end

  def download_url(filename)
    key = URI.parse(path).path.gsub(%r{\A\/}, '')
    s3 = Aws::S3::Client.new
    s3_client = Aws::S3::Client.new

    obj = Aws::S3::Object.new(
      ENV['S3_BUCKET'], key
    )

    obj.presigned_url(:get, expires_in: 3600, response_content_disposition: "attachment; filename=#{filename}")
  end

  private

  def resize_temp_file(temp_img_path)
    image = MiniMagick::Image.new(temp_img_path)
    image.combine_options do |img|
      img.resize '200x200>'
    end
  end

  def store_image_temp_location
    key = URI.parse(path).path.gsub(%r{\A\/}, '')
    s3 = Aws::S3::Client.new
    response = s3.get_object(bucket: ENV['S3_BUCKET'], key: key)
    name = File.basename(key)
    temp_file_location = "./tmp/#{name}"
    File.open(temp_file_location, 'wb') { |file| file.write(response.body.read) }
    temp_file_location
  end

  def upload_thumbnail(temp_img_path)
    s3 = Aws::S3::Resource.new
    bucket = ENV['S3_BUCKET']
    obj = s3.bucket(bucket).object('thumbnails/' + name)
    obj.upload_file(temp_img_path, acl: 'public-read')
    obj.public_url
  end
end

# frozen_string_literal: true

class User < ApplicationRecord
  attr_accessor :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest

  validates :name, presence: true, length: { maximum: 50 }, unless: :guest?
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email, presence: true,
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false, scope: :guest }, unless: :guest?
  VALID_PHONE_NUMBERS_REGEX = /\A01[0-2]{1}[0-9]{8}/.freeze

  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true, unless: :guest?

  validates_confirmation_of :password, allow_nil: true, unless: :guest?

  has_secure_password(validations: false)
  has_many :photos
  has_many :orders
  has_one :cart
  has_many :addresses

  def self.new_guest
    new { |u| u.guest = true }
  end

  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
      BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def self.new_token
    SecureRandom.urlsafe_base64
  end

  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def send_confirm_order_email(order)
    UserMailer.send_order_confirmation(self, order).deliver_now
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?

    BCrypt::Password.new(digest).is_password?(token)
  end

  def activate
    update_attributes(activated: true)
    update_attributes(activated_at: Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def move_to(non_guest_account)
    photos.update_all(user_id: non_guest_account.id)
    cart.move_items_to_other_user_cart(non_guest_account) if cart.present?
    orders.update_all(user_id: non_guest_account.id)
    addresses.update_all(user_id: non_guest_account.id)
  end

  private

  def downcase_email
    self.email = email.downcase if email?
  end
end

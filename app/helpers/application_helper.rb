module ApplicationHelper
	def full_title(page_title = "")
		base_title = "The Easiest way to print and frame photos in Egypt"
		if base_title.empty?
			base_title
		else
			page_title + " | " + base_title
		end
	end

	def show_svg(path)
		File.open("app/assets/images/#{path}", "rb") do |file|
		  raw file.read
		end
	end
end

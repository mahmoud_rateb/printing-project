# frozen_string_literal: true

module OrdersHelper
  def frame_css_class(frame)
    if !frame
      nil
    elsif frame.name == 'Bold'
      'bold_frame'
    elsif frame.name == 'Classic'
      'classic_frame'
    elsif frame.name == 'Clean'
      'clean_frame'
    else frame.name == 'Ever'
      'ever_frame'
    end
  end

  def photo_without_frames_grouped_options
    unframed_photo_sizes = PhotoSize.all.select('size, selling_price, id').where(:is_frame => [false, nil]).order(:selling_price)
    photos_without_frames_sizes = []
    unframed_photo_sizes.each do |photo_size|
      photos_without_frames_sizes.push([photo_size.size_and_price, photo_size.id])
    end
    grouped_options = {'Photo without frame' => photos_without_frames_sizes}
  end

  def non_matted_framed_photo_grouped_options
    framed_photo_sizes = FrameSize.all.where(:is_matted => false).order(:selling_price)
    frame_with_photo_sizes = []
    framed_photo_sizes.each do |framed_size|
      frame_with_photo_sizes.push([framed_size.size_and_price, framed_size.id])
    end
    grouped_options = {
      'Frame with photo' => frame_with_photo_sizes
    }
  end

  def matted_framed_photos_grouped_options
    framed_photo_sizes = FrameSize.all.where(:is_matted => true).order(:selling_price)
    frame_with_photo_sizes = []
    framed_photo_sizes.each do |framed_size|
      frame_with_photo_sizes.push([framed_size.size_and_price, framed_size.id])
    end
    grouped_options = {
      'Frame with photo' => frame_with_photo_sizes
    }
  end

end

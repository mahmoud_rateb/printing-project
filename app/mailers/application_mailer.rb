class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@printpicsnow.com'
  layout 'mailer'
end

# frozen_string_literal: true

class OrderMailer < ApplicationMailer
  def printer_photos(order)
    sizes_to_print = {}
    all_order_items = order.order_items.where('quantity > 0')
    items_without_frames = all_order_items.joins(:photo_size).where(photo_sizes: {is_frame:[nil, false]})
    items_without_frames.each do |item|
      size = item.photo_size.size
      sizes_to_print[size] = [] unless sizes_to_print.key? size
      sizes_to_print[size] << item
    end

    @links = {}
    sizes_to_print.each do |size, items_array|
      photo_counter = 1
      items_array.each do |item|
        quan_of_photo = item.quantity
        quan_of_photo.times do
          attachment_name = "#{size}-#{photo_counter}"
          photo_counter += 1
          @links[attachment_name] = item
        end
      end
    end

    mail to: 'rateb@aucegypt.edu', subject: 'Mahmoud Rateb'
  end
end

class StaticPagesController < ApplicationController
  def home
  	@file = current_user.photos.build if logged_in?
  end

  def help
  end

  def about
  end
end

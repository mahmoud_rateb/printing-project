# frozen_string_literal: true

class AddressesController < ApplicationController
  before_action :logged_in_user, except: []
  def create
    @user = current_user
    @address = @user.addresses.new(address_params)
    if @address.save
      flash[:success] = 'Address has been saved'
      redirect_back_to shipping_path
    else
      render 'addresses/new'
    end
  end

  def new
    @address = current_user.addresses.new
  end

  def show; end

  def index
    @addresses = current_user.addresses
  end

  def edit; end

  def update; end

  def destroy; end

  private

  def address_params
    params.require(:address).permit(:name, :area_id, :street_name, :building_no,
                                    :mobile_no, :flat, :floor, :landmark)
  end
end

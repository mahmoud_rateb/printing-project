class PhotosController < ApplicationController
  before_action :logged_in_user

  def new
    @file = current_user.photos.build if logged_in?
  end

  def index
    @user = current_user
    @photo_sizes = PhotoSize.all
  end

  def add_photos
    photos = JSON.parse(params[:photos])
    uploaded_photos_ids = []
    photos.each do |uploaded_photo|
      stored_photo = current_user.photos.create(uploaded_photo)
      # stored_photo.store_thumbnail_in_s3
      uploaded_photos_ids << stored_photo[:id]
    end
    session[:uploaded_photos_ids] = uploaded_photos_ids
    redirect_to orders_choose_sizes_path
  end

  def photo_params
    params.permit(:photos)
  end

end

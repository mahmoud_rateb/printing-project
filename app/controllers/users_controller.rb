# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :logged_in_user, only: %i[edit update]
  before_action :correct_user, only: %i[edit update]

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = params[:user] ? User.new(user_params) : User.new_guest
    if @user.save
      if @user.guest?
        login(@user)
        redirect_to upload_path
      else
        current_user.move_to(@user) if current_user &.guest?
        @user.send_activation_email
        redirect_to activate_path
      end
    else
      render 'new'
    end
  end

  def activate

  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = 'The account has been updated!'
      redirect_to edit_user_path
    else
      render 'edit'
    end
  end
end

private

def user_params
  params.require(:user).permit(:name, :email, :mobile_no, :password, :password_confirmation,
                               :study_year)
end

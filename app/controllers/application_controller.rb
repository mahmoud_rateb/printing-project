# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  before_action :set_locale

  def default_url_options
    { locale: I18n.locale }
  end

  private

  def logged_in_user
    return if logged_in?

    store_location
    flash[:danger] = 'Please log in to access this page'
    redirect_to login_url
  end

  def correct_user
    @user = User.find(params[:id])
    if @user != current_user
      flash[:danger] = "You are not authorized to access the page you request.
       Please login with the correct credentials and try again"
    end
    redirect_to(root_url) unless @user == current_user
  end

  def cart_is_not_empty
    unless current_user.cart && current_user.cart.order_items.where('quantity > 0').any?
      redirect_to cart_path
    end
  end

  def admin_user
    if !current_user.admin?
      flash[:danger] = 'You do not have the proper authorization to access this page. Please get proper credentials and try again'
      redirect_to(root_url) unless current_user.admin?
    end
  end

  def set_locale
    locale = params[:locale].to_s.strip.to_sym
    I18n.locale = I18n.available_locales.include?(locale) ?
        locale :
        I18n.default_locale
  end

  def create_guest_if_needed
    return if session[:user_id] # already logged in, don't need to create another one
    @user = User.new_guest
    @user.save
    session[:user_id] = @user.id
  end

end

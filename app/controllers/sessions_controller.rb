class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user &.authenticate(params[:session][:password])
      if user.activated?
        if current_user &.guest?
          current_user.move_to(user)
          login user
          redirect_to cart_path
        else
          login user
          redirect_back_to root_path
        end
      else
        account_not_activated_error
        redirect_to root_path
      end
    else
      flash.now[:danger] = t("login.invalid_login")
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

  private
  def account_not_activated_error
    msg = "Account has not been activated. "
    msg += "Please check your inbox to activate your account"
    flash[:warning] = msg
  end
end

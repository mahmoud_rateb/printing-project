class AccountActivationsController < ApplicationController
  def edit
    @user = User.find_by(email: params[:email])
    if @user && !@user.activated? && @user.authenticated?(:activation,params[:id])
      @user.activate
      login @user
      flash[:success] = "Account activated! Welcome again to Printpicsnow!"
      redirect_to upload_path
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_path
    end
  end
end

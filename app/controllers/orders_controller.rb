# frozen_string_literal: true

class OrdersController < ApplicationController
  before_action :logged_in_user, except: [:cart]
  before_action :create_guest_if_needed, only: [:cart]
  before_action :cart_is_not_empty, only: %i[checkout confirm choose_address]
  before_action :admin_user, only: %i[backend_dashboard backend_order]
  before_action :fetch_cart, only: %i[update_quantity remove_item checkout
                                      set_address confirm place_order]
  before_action :fetch_current_user, only: %i[add_to_cart choose_sizes cart
                                              set_email checkout_guest choose_address]

  def add_to_cart
    @cart = @user.cart || @user.build_cart

    @cart.attributes = add_to_cart_params

    @cart.order_items.reject(&:persisted?).each do |order_item|
      if order_item.frame_id
        order_item.frame_size_id = order_item.item_size_id
        order_item.selling_price = order_item.frame_size.selling_price
      else
        order_item.photo_size_id = order_item.item_size_id
        order_item.selling_price = order_item.photo_size.selling_price
      end
    end
    if @cart.save
      redirect_to cart_url
    else
      abort(@cart.errors.inspect)
    end
  end

  def choose_sizes
    if session[:uploaded_photos_ids]
      @order = @user.orders.build
      @frames = Frame.all
      uploaded_photos_ids = session[:uploaded_photos_ids]
      @uploaded_photos = Photo.find uploaded_photos_ids
      session.delete(:uploaded_photos_ids)
      render '/orders/choose_photo_sizes'
    else
      flash[:danger] = 'You need to upload photos before choosing sizes'
      redirect_to upload_path

      # Comment from else till here, and uncomment whats below if you want to test the
      # choose_photo_sizes page correctly

      # @user = current_user
      # @order = @user.orders.build
      # @frames = Frame.all
      # uploaded_photos_ids = session[:uploaded_photos_ids]
      # @uploaded_photos = Photo.find([796,795,794])
      # session.delete(:uploaded_photos_ids)
      # render '/orders/choose_photo_sizes'
    end
  end

  def cart
    @cart = @user.cart
  end

  def update_quantity
    order_item_id = update_quantity_params[:order_item_id]
    sent_order_item = update_quantity_params[:cart][:order_items_attributes].to_hash.values[0]

    quantity = sent_order_item['quantity']
    @cart_item = OrderItem.find(order_item_id)
    @cart_item.quantity = quantity
    @cart_item.save
    if @cart.order_items.where('quantity > 0').any?
      respond_to do |format|
        format.html { redirect_to cart_path }
        format.js
      end
    else
      redirect_to cart_path
    end
  end

  def remove_item
    cart_item = OrderItem.find(remove_item_params[:order_item_id])
    cart_item.quantity = 0
    cart_item.save
    if @cart.order_items.where('quantity > 0').any?
      respond_to do |format|
        format.html { redirect_to cart_path }
        format.js
      end
    else
      redirect_to cart_path
    end
  end

  def checkout
    @cart.update_attributes(checkout_params)

    abort(@cart.errors.inspect) unless @cart.save

    if current_user.guest?
      redirect_to checkout_guest_path
    elsif current_user.addresses.any?
      redirect_to shipping_path
    else
      redirect_to new_address_path
    end
  end

  def checkout_guest; end

  def set_email
    @user.email = params.require(:user).permit(:email)[:email]
    @user.save
    redirect_to new_address_path
  end

  def choose_address
    @order = current_user.cart
    @addresses = current_user.addresses
    if @addresses
      render 'choose_address'
    else
      @address = current_user.addresses.build
      render 'addresses/new'
    end
  end

  def set_address
    return unless @cart

    @cart.address_id = set_address_params[:address_id]
    @cart.save
    redirect_to confirm_path
  end

  def confirm
    render 'confirm'
  end

  def place_order
    return unless @cart.place_order

    current_user.send_confirm_order_email(@cart)
    redirect_to success_path
  end

  def successful_order
    @order = current_user.orders.last
    render 'success'
  end

  def change_status
    @order = Order.find(change_status_params[:order_id])
    @order.status = change_status_params[:status]

    respond_to do |format|
      if @order.save
        format.html { redirect_to backend_dashboard_path, notice: 'Status has been changed' }
        format.js
      else
        format.html { redirect_to backend_dash_path, notice: 'An error occured. Status was not changed' }
      end
    end
  end

  def send_printing_email
    order_id = send_printing_email_params[:order_id]
    order = Order.find(order_id)
    order.send_printing_email
    flash[:success] = 'Mail has been sent'
    redirect_to backend_dashboard_path
  end

  def backend_dashboard
    @orders = if filter_dashboard_params[:status]
                Order.where(status: filter_dashboard_params[:status])
              else
                Order.where.not(status: 0)
              end
  end

  def backend_order
    @order = Order.find(params[:id])
  end

  def add_to_cart_params
    params.require(:order).permit(order_items_attributes: %i[item_size_id frame_id photo_id quantity])
  end

  def checkout_params
    params.require(:cart).permit(order_items_attributes: %i[id quantity])
  end

  def update_quantity_params
    params.permit({ cart: { order_items_attributes: %i[id quantity] } }, :order_item_id)
  end

  def set_address_params
    params.require(:cart).permit(:address_id)
  end

  def change_status_params
    params.require(:order).permit(:order_id, :status)
  end

  def remove_item_params
    params.permit(:order_item_id)
  end

  def send_printing_email_params
    params.permit(:order_id)
  end

  def filter_dashboard_params
    params.permit(status: [])
  end

  private

  def fetch_cart
    @cart = current_user.cart
  end

  def fetch_current_user
    @user = current_user
  end
end

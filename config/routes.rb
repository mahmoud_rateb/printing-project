Rails.application.routes.draw do

  scope "(:locale)", locale: /en|ar/ do
    get 'errors/not_found'

    get 'errors/unacceptable'

    get 'errors/internal_error'

    get 'addresses/create'

    get 'addresses/show'

    get 'addresses/index'

    get 'addresses/edit'

    get 'addresses/update'

    get 'addresses/destroy'

    get 'sessions/new'

    get 'users/new'

    root 'static_pages#home'
    get '/help', to: 'static_pages#help'
    get '/about', to: 'static_pages#about'
    get '/frames', to: 'static_pages#frames'
    get '/prints', to: 'static_pages#prints'
    get '/signup', to: 'users#new'
    post '/signup', to: 'users#create'
    get '/activate', to: 'users#activate'
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'

    resources :users
    resources :addresses
    resources :photos, only: [:create, :destroy, :index]
    resources :password_resets, only: [:new, :edit, :create, :update]
    post 'photos/add_photos'
    get '/upload', to: 'photos#new'
    resources :account_activations, only: [:edit]
    post 'orders/add_to_cart'
    get 'orders/choose_sizes'
    get 'orders/backend_dashboard', as: 'backend_dashboard'
    get 'orders/backend_dashboard/:id', to: 'orders#backend_order', as: 'backend_order'
    get 'confirm', to: 'orders#confirm'
    get 'success', to: 'orders#successful_order'
    get 'shipping', to: 'orders#choose_address'
    get 'checkout_guest', to: 'orders#checkout_guest'
    get 'send_printing_email', to: 'orders#send_printing_email'
    patch 'set_address', to: 'orders#set_address'
    patch 'place_order', to: 'orders#place_order'
    patch 'checkout', to: 'orders#checkout'
    patch 'remove_item', to: 'orders#remove_item'
    patch 'update_quantity', to: 'orders#update_quantity'
    patch 'change_status', to: 'orders#change_status'
    patch 'set_guest_email', to: 'orders#set_email'
    get '/cart', to: 'orders#cart'

    mount UPPY_S3_MULTIPART_APP => "/s3/multipart"

    # Errors
    get "/404", :to => "errors#not_found"
    get "/422", :to => "errors#unacceptable"
    get "/500", :to => "errors#internal_error"
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  end
end

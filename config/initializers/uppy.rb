require "uppy/s3_multipart"

UPPY_S3_MULTIPART_APP = Uppy::S3Multipart::App.new(bucket: S3_BUCKET,
	prefix: "uploads3", public: true)
